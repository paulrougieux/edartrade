---
title: "EU trade in logs, sawnwood and plywood"
author: "Paul"
date:  "`r format(Sys.time(), '%d %B, %Y')`"
output:
  pdf_document: 
    toc: yes
geometry: margin=0.5cm
---

```{r setup, include=FALSE}
# The following command renders this notebook at a bash prompt
# cd ~/rp/edartrade/notebooks/product_analysis &&  Rscript -e "rmarkdown::render('rwd_swd_ply.Rmd', 'pdf_document')"

knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(fig.width=12, fig.height=9, warning=FALSE)
library(kableExtra)
library(knitr)
opts_knit$set(root.dir="../..") # file paths are relative to the root of the project directory
library(dplyr)
library(tidyr)
library(ggplot2)
library(eutradeflows)
# Data frame to add flow description for plots and table output 
flow_name <- tibble(flow = c("Import","Export"), flowcode = c(1,2))
```

# Introduction

The purpose of this notebook is to analyse the trade in logs, sawnwood and
plywood with a particular focus on trade unit prices. 

CN codes have 8 digits We start with an overview at the 4 digit level for logs
(4403), sawnwood (4407) and plywood (4412). Then we display the major products
at the 8 digit level. Because product codes can change through time, there is
often a need to aggregate many products together to form a consistent time
series i.e. to be sure that changes in trade volume are not due to a change in
definition but to a real event.

Plot time series of trade value, weight and prices for the major extra EU export and
import partners. 

- TODO: plot trade value weight prices of 4 digit products for intra vs extra
  EU for rwd swd ply. In addition plot these for the major trade partners.

- TODO: Select the main products at 8 digit levels, including changes in
  product codes if necessary. Build time series of Oak, beech, spruce pine rwd
  and swd and of the major plywood products.

- TODO: [4:44 PM] ROUGIEUX Paul (JRC-ISPRA) I've seen they display the consumer
  price of bread and cereals in this agricultural market dashboard. I think we
  should add a wood construction or related price index to the prices series
  https://ec.europa.eu/info/sites/default/files/food-farming-fisheries/farming/documents/commodity-price-dashboard_2021-05_en.pdf 


## See also

See also the following notebooks

- notebooks/issues/product_changes.Rmd for an analysis of the changes in
  product codes.


## Data preparation

Bilateral trade data was downloaded from the Eurostat Comext bulk download
facility and filtered for wood products (CN codes starting with 44) and
furniture (94). There is no data for the UK after January 2020. The UK was
removed from the dataset and EU aggregates for the whole time series mean EU 27
without the UK.



# Overview at the 4 digit level

```{r}
# Load trade flows data, update these rds files with R/move_from_db_to_csv_and_rds_files.R
wood_trade <- readRDS("data/trade_flows/eu_trade_44.rds") %>% 
    mutate(year = lubridate::year(period), 
           productcode4d = substr(productcode, 1, 4)) %>%
    # Important to remove the UK here
    filter(reporter != "United Kingdom")
reporter_countries <- distinct(wood_trade, reporter, reportercode)
partner_countries <- distinct(wood_trade, partner, partnercode, partneriso)
product_names <- readRDS("data/trade_flows/product_names.rds")
wood_trade <- wood_trade %>%
    mutate(intra_extra = ifelse(partnercode %in% reporter_countries$reportercode, "intra EU", "extra EU")) 

# Short product names for the products of interest
# products_of_interest <- data.frame(productcode2d = c(),
#                                    shortname = c("swd oak", "swd pine", "swd beech"),
#                                    stringsAsFactors = FALSE)

product_names_4d <- data.frame(productcode4d = c("4403", "4407", "4412"),
                               short_name = c("rwd", "swd", "ply"), 
                               stringsAsFactors = FALSE)
```

\newpage


## Plot value, quantity and prices

We aggregate trade value and weight across reporters at the 4 digit levels,
then compute the unit price of trade. This enables us to illustrate the inter
country variability of price series on the same plot. An aggregate price series
for the whole EU is displayed in bold on the same plot.

```{r}
# Aggregate trade values and weight for each separate reporter and group products at the 4 digit level
# Reshape to long format so that value and weight variables can be placed in facets.
trade_variables <- c("tradevalue", "weight", "quantity")
wt_4d_by_reporter <- wood_trade %>%
    group_by(flowcode, reporter, productcode4d, period) %>%
    summarise(across(all_of(trade_variables),
                     sum, na.rm=TRUE), .groups = "drop") %>%
    group_by(flowcode, productcode4d) %>%
    mutate(tradevalue = tradevalue / 1e6,
           quantity = quantity / 1e6,
           weight = weight / 1e6,
           pricew = tradevalue / weight * 1000, 
           # Replace price outliers by NA values so that the plot scale stays
           # below a reasonable limit
           pricew = ifelse(pricew > 2e3, NA, pricew),
           pricew = ifelse(productcode4d == "4403" & pricew > 500, NA, pricew)) %>%
    pivot_longer(all_of(c(trade_variables, "pricew")),
                 names_to = "variable", values_to = "value") %>%
    left_join(product_names_4d, by = "productcode4d") %>%
    unite("product_variable", short_name, variable, remove = FALSE) %>%
    ungroup() %>%
    arrange(flowcode, variable, productcode4d) %>%
    mutate(product_variable = forcats::as_factor(product_variable))

# Aggregate trade values and weight for the whole EU as one reporter and group products at the 4 digit level
wt_4d_eu <- wood_trade %>%
    group_by(flowcode, productcode4d, period) %>%
    summarise(across(all_of(trade_variables),
                     sum, na.rm=TRUE), .groups = "drop") %>%
    mutate(tradevalue = tradevalue / 1e6,
           quantity = quantity / 1e6,
           weight = weight / 1e6,
           pricew = tradevalue / weight * 1000, 
           # Replace price outliers by NA values so that the plot scale stays
           # below a reasonable limit
           pricew = ifelse(pricew > 2e3, NA, pricew),
           pricew = ifelse(productcode4d == "4403" & pricew > 500, NA, pricew),
           reporter = "EU") %>%
    pivot_longer(all_of(c(trade_variables, "pricew")),
                 names_to = "variable", values_to = "value") %>%
    left_join(product_names_4d, by = "productcode4d") %>%
    unite("product_variable", short_name, variable, remove = FALSE) %>%
    ungroup() %>%
    arrange(flowcode, variable, productcode4d) %>%
    mutate(product_variable = forcats::as_factor(product_variable))
```

### EU

```{r}
wt_4d_eu %>%
    filter(flowcode == 1) %>%
    ggplot(aes(x = period, y = value)) +
    geom_line() +
    facet_wrap(~ product_variable, ncol = 3, scales = "free") +
    ggtitle("Import volumes and prices of roundwood 4403, sawnwood 4407 and plywood 4412",
    subtitle = "Trade value in million EUR, quantity in thousand m3, weight in thousand tons and prices in EUR/tons")
```

```{r}
wt_4d_eu %>%
    filter(flowcode == 2) %>%
    ggplot(aes(x = period, y = value)) +
    geom_line() +
    facet_wrap(~ product_variable, ncol = 3, scales = "free") +
    ggtitle("Export volumes and prices of roundwood 4403, sawnwood 4407 and plywood 4412",
    subtitle = "Trade value in million EUR, quantity in thousand m3, weight in thousand tons and prices in EUR/tons")
```


### By country

```{r}
wt_4d_by_reporter %>%
    filter(flowcode == 1) %>%
    ggplot(aes(x = period, y = value, color = reporter)) +
    geom_line(aes(color = reporter)) +
    # Add EU price series
    geom_line(data = filter(wt_4d_eu, variable == "pricew", flowcode == 1), color = "black") +
    facet_wrap(~ product_variable, ncol = 3, scales = "free") +
    ggtitle("Import volumes and prices of roundwood 4403, sawnwood 4407 and plywood 4412",
    subtitle = "Trade value in million EUR, quantity in thousand m3, weight in thousand tons and prices in EUR/tons")
```

```{r}
wt_4d_by_reporter %>%
    filter(flowcode == 2) %>%
    ggplot(aes(x = period, y = value, color = reporter)) +
    geom_line(aes(color = reporter)) +
    # Add EU price series
    geom_line(data = filter(wt_4d_eu, variable == "pricew", flowcode == 2), color = "black") +
    facet_wrap(~ product_variable, ncol = 3, scales = "free") +
    ggtitle("Export volumes and prices of roundwood 4403, sawnwood 4407 and plywood 4412",
    subtitle = "Trade value in million EUR, quantity in thousand m3, weight in thousand tons and prices in EUR/tons")
```


### Prices only

```{r}
wt_4d_by_reporter %>%
    filter(variable == "pricew") %>%
    ggplot(aes(x = period, y = value)) +
    geom_line(aes(color = reporter)) +
    # Add EU price series
    geom_line(data = filter(wt_4d_eu, variable == "pricew"), color = "black") +
    facet_grid(short_name ~ flowcode  , scales = "free") +
    ggtitle("Import (1) and export (2) prices of roundwood 4403, sawnwood 4407 and plywood 4412",
    subtitle = "Prices in EUR/tons, EU aggregate in black")
```


### Consumer price index


\newpage

## Table of EU Export quantity aggregated at the 4 digit level

List of reporter countries with the average **export** value over the last 5 years
Value in million Euros and weight in 1000 tons.

```{r}
wt_overview <- wood_trade %>%
    # Keep the last full 5 years of data
    filter(max(year) - 5 <= year & year < max(year)) %>%
    group_by(flowcode, reporter, productcode4d) %>%
    summarise(tradevalue = sum(tradevalue, na.rm = TRUE)/5e6,
              weight = sum(weight, na.rm = TRUE)/5e6, 
              .groups = "drop") %>%
    pivot_longer(tradevalue:weight, names_to = "variable") %>%
    mutate(variable = gsub("trade", "", variable)) %>%
    left_join(product_names_4d, by = "productcode4d") %>%
    unite("name_var", short_name, variable) %>%
    pivot_wider(-productcode4d, names_from = "name_var", values_from = "value") %>%
    arrange(reporter)
    
wt_overview %>%
    filter(flowcode == 2) %>%
    select(-flowcode) %>%
    kable(digits = 0, caption = "average export values over the last 5 years
Value in million Euros and weight in 1000 tons")
```


\newpage

## Table of EU Import quantity aggregated at the 4 digit level

List of reporter countries with the average **export** value over the last 5 years
Value in million Euros and weight in 1000 tons.

```{r}
wt_overview %>%
    filter(flowcode == 1) %>%
    select(-flowcode) %>%
    kable(digits = 0, caption = "average import values over the last 5 years
Value in million Euros and weight in 1000 tons")
```


```{r}
wt_overview %>%
    filter(flowcode == 1) %>%
    kable(digits = 0, caption = "average import values over the last 5 years
Value in million Euros and weight in 1000 tons")
```


## Table of EU prices aggregated at the 4 digit level

```{r}
wt_overview_prices <- wood_trade %>%
    # Keep the last full 5 years of data
    filter(max(year) - 5 <= year & year < max(year)) %>%
    mutate(tradevaluelast = ifelse(year == max(year), tradevalue, 0),
           weightlast = ifelse(year == max(year), weight, 0)) %>%
    group_by(flowcode, reporter, productcode4d) %>%
    # Average over 5 years
    summarise(tradevalue = sum(tradevalue, na.rm = TRUE)/5,
              weight = sum(weight, na.rm = TRUE)/5, 
              # Sum only for the last year
              tradevaluelast = sum(tradevaluelast, na.rm = TRUE),
              weightlast = sum(weightlast, na.rm = TRUE),
              .groups = "drop") %>%
    mutate(pricew = tradevalue / weight * 1e3, 
           pricewlast = tradevaluelast / weightlast * 1e3) %>%
    left_join(product_names_4d, by = "productcode4d") %>%
    select(flowcode, reporter, short_name, contains("price")) %>%
    pivot_longer(contains("price"), names_to = "variable") %>%
    unite("name_var", short_name, variable) %>%
    pivot_wider(names_from = "name_var", values_from = "value")

wt_overview_prices %>%
    filter(flowcode == 2) %>%
    kable(digits = 0, caption = "average export prices over the last 5 years
          compared to the last year. Prices in euros per ton.")
```

```{r}
wt_overview_prices %>%
    filter(flowcode == 1) %>%
    kable(digits = 0, caption = "Average import prices over the last 5 years
          compared to the last year. Prices in euros per ton.")
```


\newpage

# Major products traded over the last 5 years

Compute yearly average over the last 5 complete years.
Keep only the major products exported or imported, inside or outside the EU (First 10 products under each of those 4 groups).

```{r}
wt_major <- wood_trade %>%
    # Keep the last full 5 years of data
    filter(max(year) - 5 <= year & year < max(year)) %>%
    mutate(ief = paste(intra_extra, ifelse(flowcode == 1, "import", "export"))) %>%
    group_by(productcode4d, productcode, flowcode, intra_extra, ief) %>%
    summarise(across(all_of(c("tradevalue", "weight", "quantity")),
                     sum, na.rm=TRUE), .groups = "drop") %>%
    arrange(desc(flowcode), intra_extra, productcode4d, desc(weight)) %>%
    left_join(product_names, by = "productcode") %>%
    mutate(productdescription_short = substr(productdescription, 1, 60), 
           # Average over the last 5 years and convert to million €
           tradevalue = tradevalue/5e6, 
           # Average over the last 5 years and convert to thousand tons
           weight = weight/5e6,
           # Average over the last 5 years and convert to thousand m3
           quantity = quantity/5e3)
```


```{r, fig.pos="H"}
major_trade_table <- function(df, code4d, product_name){
    df %>%
        filter(productcode4d == code4d) %>%
        group_by(intra_extra, flowcode) %>%
        slice(1:10) %>%
        ungroup() %>% 
        arrange(productcode, intra_extra, flowcode) %>%
        select(productcode, ief, tradevalue, weight, quantity, productdescription) %>%
        kable(format = "latex", digits = 0, booktabs = TRUE, longtable = TRUE, 
              caption = sprintf("Major %s products", product_name)) %>%
        kableExtra::column_spec(6, width = "25em") %>%
        kableExtra::collapse_rows(columns = 6, latex_hline = "major", valign = "middle")
}

major_trade_table(wt_major, "4407", "sawnwood")
```


```{r}
major_trade_table(wt_major, "4403", "roundwood")
```

```{r}
major_trade_table(wt_major, "4412", "plywood")
```

\newpage

# Main Extra EU export partners


List the main extra EU export partners for the major commodities in the last 5
years.

```{r}
wood_trade %>%
    filter(intra_extra == "extra EU" & flowcode == 2) %>%
    filter(max(year) - 5 <= year & year < max(year)) %>%
    group_by(partner, productcode4d) %>%
    summarise(across(all_of(c("tradevalue", "weight", "quantity")),
                     function(x) sum(x, na.rm=TRUE)/5), .groups = "drop") %>%
    left_join(product_names_4d, by = "productcode4d") %>%
    group_by(short_name) %>%
    arrange(desc(weight)) %>%
    slice(1:10) %>%
    select(short_name, everything(), -productcode4d) %>%
    # Replace long name containing "not specified" with a shorter one
    mutate(partner = ifelse(grepl("not specified", partner), 
                            "not specified", partner)) %>%
    kable() %>%
    kableExtra::collapse_rows(columns = 1, latex_hline = "major", valign = "middle")
```

```{r}
########################################
# Functions to reuse in many countries #
########################################

#' Select the major products for a given partner country and flow direction
#' @decription Aggregate trade value and weight over the last n full years
#' (defined by the year_interval) and order by decreasing weight. 
#' @param df data frame of trade flows
#' @param partner_ character name of a partner country
#' @param flowcode_ numeric code of a trade flow
#' @param product_names_ dataframes mapping product codes to product names
#' @return 
select_major_products <- function(df, partner_, flowcode_,
                                  year_interval = 5,
                                  product_names_ = product_names){
    df %>%
        filter(partner == partner_ & flowcode == flowcode_) %>%
        filter(max(year) - year_interval <= year & year < max(year)) %>%
        group_by(flowcode, partner, productcode) %>%
        summarise(across(all_of(c("tradevalue", "weight", "quantity")),
                         function(x) sum(x, na.rm=TRUE)/5), .groups = "drop") %>%
        left_join(product_names_, by = "productcode") %>%
        arrange(desc(weight)) 
}



#' Display a table of the n first major products
#' @param df data frame output of select_major_products()
display_table_of_major_products <- function(df, n = 10, product_prefix = ""){
    flow_verb <- ifelse(unique(df$flowcode) == 1, 
                        "imported from", "exported to")
    partner_country_name <- unique(df$partner)
    df %>%
        slice(1:10) %>%
        select(-flowcode, -partner) %>%
        kable(caption = sprintf("%s major %s products %s %s", 
                                n, product_prefix, flow_verb,  partner_country_name)) %>%
        kableExtra::column_spec(5, width = "30em")
}


#' Plot tradevalue, weight and prices side by side in a facet plot
#' @description Reshape to long format, then generated a facetted plot
#' for the given partner and product codes.
#' @param df a data frame of trade flows (the whole wood_trade is fine)
#' @param productcode_ a vector of product codes
plot_value_weight_price <- function(df, partner_, productcode_, title_suffix = ""){
    df_long <- df %>%
        filter(partner == "China" & productcode %in% productcode_) %>%
        group_by(partner, productcode, period) %>%
        summarise(across(all_of(c("tradevalue", "weight", "quantity")),
                         # Trade value in million EUR and weight in million kg
                         function(x) sum(x, na.rm=TRUE)/1e6), .groups = "drop") %>%
        mutate(pricew = tradevalue / weight * 1e3 ) %>%
        select(-quantity) %>%
        pivot_longer(tradevalue:pricew, names_to = "variable")
    df_long %>%
        ggplot(aes(x = period, y = value)) +
        geom_line() +
        facet_wrap(productcode ~ variable, scales = "free_y", ncol = 3) +
        ylab("") + 
        ggtitle(sprintf("EU exports to %s %s", partner_, title_suffix), 
                subtitle = 'Prices in EUR/ton, trade value in million EUR and weight in thousand tons')
}
```

\newpage

## China

Main products exported to China in the last 5 years at the 8 digit level.
Average yearly trade over the last 5 years.

```{r}
wt_china_major_swd_export <- wood_trade %>%
    filter(productcode4d == "4407") %>%
    select_major_products("China", 2)

wt_china_major_swd_export %>%
    display_table_of_major_products(product_prefix = "sawnwood")
```

Trade value, weight and price plots for the first 10 commodities exported from the EU to China.

```{r}
# Separate in 2 plots with 5 products each
wood_trade %>%
    plot_value_weight_price("China", wt_china_major_swd_export$productcode[1:5])
```

```{r}
wt_china_major_rwd_export <- wood_trade %>%
    filter(productcode4d == "4403") %>%
    select_major_products("China", 2)

wt_china_major_rwd_export %>%
    display_table_of_major_products(product_prefix = "roundwood")
```

```{r}
wood_trade %>%
    plot_value_weight_price("China", wt_china_major_rwd_export$productcode[1:5])
```

\newpage

## Egypt

Main products exported to Egypt in the last 5 years at the 8 digit level.
Average yearly trade over the last 5 years.

```{r}
wt_eg_major_export <- wood_trade %>%
    select_major_products("Egypt", 2)

wt_eg_major_export %>%
    display_table_of_major_products()
```

Trade value, weight and price plots for the first 10 commodities exported from the EU to Egypt.

```{r}
# Separate in 2 plots with 5 products each
wood_trade %>%
    plot_value_weight_price("Egypt", wt_eg_major_export$productcode[1:5])
```

```{r}
# Separate in 2 plots with 5 products each
wood_trade %>%
    plot_value_weight_price("Egypt", wt_eg_major_export$productcode[6:10])
```


\newpage

## United Kingdom

Main products exported to United Kingdom in the last 5 years at the 8 digit level.
Average yearly trade over the last 5 years.

```{r}
wt_uk_major_export <- wood_trade %>%
    select_major_products("United Kingdom", 2)

wt_uk_major_export %>%
    display_table_of_major_products()
```

Trade value, weight and price plots for the first 10 commodities exported from the EU to United Kingdom.

```{r}
# Separate in 2 plots with 5 products each
wood_trade %>%
    plot_value_weight_price("United Kingdom", wt_uk_major_export$productcode[1:5])
```

```{r}
# Separate in 2 plots with 5 products each
wood_trade %>%
    plot_value_weight_price("United Kingdom", wt_uk_major_export$productcode[6:10])
```





\newpage

## USA

Main products exported to United States in the last 5 years at the 8 digit level.
Average yearly trade over the last 5 years.

```{r}
wt_usa_major_export <- wood_trade %>%
    select_major_products("United States", 2)

wt_usa_major_export %>%
    display_table_of_major_products()
```

Trade value, weight and price plots for the first 10 commodities exported from the EU to United States.

```{r}
# Separate in 2 plots with 5 products each
wood_trade %>%
    plot_value_weight_price("United States", wt_usa_major_export$productcode[1:5])
```

```{r}
# Separate in 2 plots with 5 products each
wood_trade %>%
    plot_value_weight_price("United States", wt_usa_major_export$productcode[6:10])
```


\newpage

# Main extra EU import partners

List the main extra coutnries from which the EU imported roundwood, sawnwood and
plywood in the last 5 years.

```{r}
wood_trade %>%
    filter(intra_extra == "extra EU" & flowcode == 1) %>%
    filter(max(year) - 5 <= year & year < max(year)) %>%
    group_by(partner, productcode4d) %>%
    summarise(across(all_of(c("tradevalue", "weight", "quantity")),
                     function(x) sum(x, na.rm=TRUE)/5), .groups = "drop") %>%
    left_join(product_names_4d, by = "productcode4d") %>%
    group_by(short_name) %>%
    arrange(desc(weight)) %>%
    slice(1:10) %>%
    select(short_name, everything(), -productcode4d) %>%
    kable() %>%
    kableExtra::collapse_rows(columns = 1, latex_hline = "major", valign = "middle")
```

\newpage

# Main intra EU import partners

We use import because the intra EU import data tends to be better than the export data.
Average intra EU trade over the last 5 years.

```{r}
wood_trade %>%
    filter(intra_extra == "intra EU" & flowcode == 1) %>%
    filter(max(year) - 5 <= year & year < max(year)) %>%
    group_by(partner, productcode4d) %>%
    summarise(across(all_of(c("tradevalue", "weight", "quantity")),
                     function(x) sum(x, na.rm=TRUE)/5), .groups = "drop") %>%
    left_join(product_names_4d, by = "productcode4d") %>%
    group_by(short_name) %>%
    arrange(desc(weight)) %>%
    slice(1:10) %>%
    select(short_name, everything(), -productcode4d) %>%
    kable() %>%
    kableExtra::collapse_rows(columns = 1, latex_hline = "major", valign = "middle")
```








```{r eval=FALSE}
# Export data to csv
write.csv(wt_overview, "~/downloads/wt_overview.csv")
write.csv(wt_overview_prices, "~/downloads/wt_overview_prices.csv")
write.csv(wt_major, "~/downloads/wt_major.csv")
```

