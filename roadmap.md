


# Overview

Before digging into particular country pairs and particular products. It is important to 
have an overview of price development in the whole EU and of wood prices in general.

- The [Harmonised Indices of Consumer 
  Prices](https://ec.europa.eu/eurostat/web/hicp/data/main-table) (HICP) can provide a 
  general idea of price developments.

- The yearly FAOSTAT time series of trade value and weight can provide an overview of 
  price developments in the last decades.

In addition when talking about export prices, it might be important to provide 
additional information on exchange rates between currencies.




