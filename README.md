

This website is a back-end for the analysis of trade in forest and agriculture 
commodities.

Folder content

- [R](R) the R code to prepare the data

- [notebooks](notebooks) statistical analysis and plots 

