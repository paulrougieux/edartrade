
# Load construction price index from Eurostat
# Loaded manually from this URL:
# https://ec.europa.eu/eurostat/databrowser/view/STS_COPI_Q__custom_1073367/default/table?lang=en


# Note: the construction price index is not available using the eurostat R package
# eurostat_code <- "STS_COPI_Q"
# dat <- get_eurostat(eurostat_code, time_format = "num")

