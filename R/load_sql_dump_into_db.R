library(eutradeflows)

sql_dump_file_name <- "raw_comext_monthly.sql.7z"


##################################################
# Download the SQL dump to a temporary directory #
##################################################
message("Downloading ", sql_dump_file_name, " to: ", tempdir())
download.file(paste0("http://edartra.de/sqldump/", sql_dump_file_name),
              file.path(tempdir(), sql_dump_file_name), 
              internet.info = 0)


#######################################
# Load the SQL dump into the database #
#######################################
# Note to avoid blocking your R session for a long time, you might want to run this lengthy code a t a separate R session.
# Empty the database table first in bash
# mariadb tradeflows
# truncate table raw_comext_monthly;
# Started around 15h15
loadtabledump("tradeflows", file.path(tempdir(), sql_dump_file_name))



